package com.itemgenerator.engine;

public enum ModifierType {
	
	RANGE, STATIC, TRIPLE;
	
}
