package com.dungeonrealms.models;

import com.dungeonrealms.Hive.Hive;
import com.dungeonrealms.enums.LogType;
import com.google.gson.JsonObject;
import com.dungeonrealms.Utils;

public class LogModel {
	
	public LogType type;
	public JsonObject data;
	public String player;
	public long time;
	
	public LogModel(LogType type, String player, JsonObject data){
		new LogModel(type, player, data, Utils.getTime());
	}
	
	public LogModel(LogType type, String player, JsonObject data, long time){
		this.type = type;
		this.player = player;
		this.data = data;
		this.time = time;
		Hive.logs.add(this);
	}
	
}
