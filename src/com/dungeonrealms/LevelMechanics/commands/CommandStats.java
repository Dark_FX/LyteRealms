package com.dungeonrealms.LevelMechanics.commands;

import com.dungeonrealms.LevelMechanics.StatsGUI.StatsGUI;
import com.dungeonrealms.LevelMechanics.StatsGUI.StatsGUIWorker;
import me.vilsol.menuengine.engine.DynamicMenuModel;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandStats implements CommandExecutor {

    
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
    	if (!(sender instanceof Player)) {
    		sender.sendMessage("You must be a player to use this command.");
    		return true;
    	}
        Player p = (Player) sender;
        DynamicMenuModel.cleanInventories(p, p.getInventory());
        StatsGUIWorker gui = (StatsGUIWorker) DynamicMenuModel.createMenu(p, StatsGUI.class);
        gui.showToPlayer(p);
        return true;
    }

}
