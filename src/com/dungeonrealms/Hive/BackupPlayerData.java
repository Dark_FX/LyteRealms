package com.dungeonrealms.Hive;

import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

import com.dungeonrealms.Main;
import com.dungeonrealms.MoneyMechanics.MoneyMechanics;
import com.dungeonrealms.ShopMechanics.ShopMechanics;

import org.bukkit.entity.Player;
import org.fusesource.jansi.Ansi;

public class BackupPlayerData extends Thread {
	List<String> player_names;
	
	public static void backupPlayers(Collection<? extends org.bukkit.entity.Player> data) {
		for(Player p : data) {
			String p_name = p.getName();
			
			if(Hive.being_uploaded.contains(p_name) || Hive.pending_upload.contains(p_name) || Hive.server_swap.containsKey(p_name)) {
				continue;
			}
			
			try {
				Hive.uploadPlayerDatabaseData(p_name);
			} catch(SQLException err) {
				err.printStackTrace();
			}
			; // Location, Inventory
			MoneyMechanics.uploadBankDatabaseData(p_name, false);
			ShopMechanics.uploadShopDatabaseData(p_name, false);
		}
	}

	public void run() {
		while(true) {
			try {
				Thread.sleep(450000 * 2);
			} catch(InterruptedException e) {}
			//  5 minute delay between syncs.

			if(!(Hive.restart_inc) && !(Hive.shutting_down) && !(ShopMechanics.shop_shutdown) && !(Hive.server_frozen)) {
				backupPlayers(Main.plugin.getServer().getOnlinePlayers());
				System.out.println("");
				System.out.println(Ansi.ansi().fg(Ansi.Color.MAGENTA).boldOff().toString() + "[Hive] Backup Query Complete. Sleeping 15m..." + Ansi.ansi().fg(Ansi.Color.WHITE).boldOff().toString());
				System.out.println("");

			}
		}
	}
}
