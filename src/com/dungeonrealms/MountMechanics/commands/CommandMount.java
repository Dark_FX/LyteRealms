package com.dungeonrealms.MountMechanics.commands;

import com.dungeonrealms.MountMechanics.MountMechanics;
import com.dungeonrealms.ShopMechanics.ShopMechanics;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Horse;
import org.bukkit.entity.Player;

public class CommandMount implements CommandExecutor {
	
	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		Player p = (Player) sender;
		
		if(!(p.isOp())) { return true; }
		
		p.getInventory().addItem(ShopMechanics.removePrice(MountMechanics.t1_mule));
		p.getInventory().addItem(ShopMechanics.removePrice(MountMechanics.t2_mule));
		p.getInventory().addItem(ShopMechanics.removePrice(MountMechanics.t2_mule_upgrade));
		p.getInventory().addItem(ShopMechanics.removePrice(MountMechanics.t3_mule_upgrade));

		return true;
	}
	
}