package com.dungeonrealms.DonationMechanics;

import com.dungeonrealms.Main;
import com.vexsoftware.votifier.model.Vote;
import com.vexsoftware.votifier.model.VotifierEvent;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

public class CustomEventListener implements Listener {
	DonationMechanics plugin = null;
	
	public CustomEventListener(DonationMechanics instance) {

		plugin = instance;
	}


	@EventHandler(priority= EventPriority.NORMAL)
	public void onVotifierEvent(VotifierEvent event) {
	    Vote vote = event.getVote();
	
	    System.out.println("[VOTE] " + vote);
	    String username = vote.getUsername();
		Main.plugin.getServer().dispatchCommand(Main.plugin.getServer().getConsoleSender(), "addec " + username + " 3");
	}
}
