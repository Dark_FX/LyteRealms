package com.dungeonrealms.DonationMechanics.commands;


import com.dungeonrealms.KarmaMechanics.KarmaMechanics;
import com.dungeonrealms.PermissionMechanics.PermissionMechanics;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandBank implements CommandExecutor {
	
		
	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		Player p = (Player) sender;
		String rank = PermissionMechanics.getRank(p.getName());
		if (KarmaMechanics.getAlignment(p.getName()).contains("good")) {
    		p.openInventory(p.getEnderChest());
    	}
    	if (rank.equalsIgnoreCase("SUB++")){
    		if (KarmaMechanics.getAlignment(p.getName()).contains("evil")) {
    			p.sendMessage(ChatColor.RED + "You cannot do this while chaotic!");
    			return true;
    		}
        if (!rank.equalsIgnoreCase("SUB++")){
			p.sendMessage(ChatColor.RED + "This feature is just for Sub++");
			return true;
        	
        	}
        	
        }
		return false;
	}
}