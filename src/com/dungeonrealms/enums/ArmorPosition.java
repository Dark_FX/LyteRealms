package com.dungeonrealms.enums;

public enum ArmorPosition {
	
	HEAD, CHEST, LEGS, BOOTS
	
}
