package com.dungeonrealms;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import net.minecraft.server.v1_8_R3.*;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftItemStack;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

public class Utils {
        public static Entity[] getNearbyEntities(Location l, int radius){
        int chunkRadius = radius < 16 ? 1 : (radius - (radius % 16))/16;
        List<Entity> radiusEntities = new ArrayList<Entity>();
            for (int chX = 0 -chunkRadius; chX <= chunkRadius; chX ++){
                for (int chZ = 0 -chunkRadius; chZ <= chunkRadius; chZ++){
                    int x=(int) l.getX(),y=(int) l.getY(),z=(int) l.getZ();
                    for (Entity e : new Location(l.getWorld(),x+(chX*16),y,z+(chZ*16)).getChunk().getEntities()){
                        if (e.getLocation().distance(l) <= radius && e.getLocation().getBlock() != l.getBlock()) radiusEntities.add(e);
                    }
                }
            }
        return radiusEntities.toArray(new Entity[radiusEntities.size()]);
    }
    
    public static long getTime(){
    	return Calendar.getInstance().getTimeInMillis();
    }
    
    public static boolean isBeta(){
    	int this_server_num = Integer.parseInt(Bukkit.getMotd().split("-")[1].split(" ")[0]);
		if(this_server_num >= 100 && this_server_num <= 110) return true;
		return false;
    }

    @Deprecated
    public static void sendTitle(Player player, Integer fadeIn, Integer stay, Integer fadeOut, String message)
    {
        sendTitle(player, fadeIn, stay, fadeOut, message, null);
    }

    @Deprecated
    public static void sendSubtitle(Player player, Integer fadeIn, Integer stay, Integer fadeOut, String message)
    {
        sendTitle(player, fadeIn, stay, fadeOut, null, message);
    }

    @Deprecated
    public static void sendFullTitle(Player player, Integer fadeIn, Integer stay, Integer fadeOut, String title, String subtitle)
    {
        sendTitle(player, fadeIn, stay, fadeOut, title, subtitle);
    }

    public static void sendTitle(Player player, Integer fadeIn, Integer stay, Integer fadeOut, String title, String subtitle)
    {
        PlayerConnection connection = ((CraftPlayer)player).getHandle().playerConnection;

        PacketPlayOutTitle packetPlayOutTimes = new PacketPlayOutTitle(PacketPlayOutTitle.EnumTitleAction.TIMES, null, fadeIn.intValue(), stay.intValue(), fadeOut.intValue());
        connection.sendPacket(packetPlayOutTimes);
        if (subtitle != null)
        {
            subtitle = subtitle.replaceAll("%player%", player.getDisplayName());
            subtitle = ChatColor.translateAlternateColorCodes('&', subtitle);
            IChatBaseComponent titleSub = IChatBaseComponent.ChatSerializer.a("{\"text\": \"" + subtitle + "\"}");
            PacketPlayOutTitle packetPlayOutSubTitle = new PacketPlayOutTitle(PacketPlayOutTitle.EnumTitleAction.SUBTITLE, titleSub);
            connection.sendPacket(packetPlayOutSubTitle);
        }
        if (title != null)
        {
            title = title.replaceAll("%player%", player.getDisplayName());
            title = ChatColor.translateAlternateColorCodes('&', title);
            IChatBaseComponent titleMain = IChatBaseComponent.ChatSerializer.a("{\"text\": \"" + title + "\"}");
            PacketPlayOutTitle packetPlayOutTitle = new PacketPlayOutTitle(PacketPlayOutTitle.EnumTitleAction.TITLE, titleMain);
            connection.sendPacket(packetPlayOutTitle);
        }
    }

    public static void actionBarMsg(Player p, String msg)
    {
        try
        {
            CraftPlayer cp = (CraftPlayer) p;
            IChatBaseComponent iChatBaseComponent = IChatBaseComponent.ChatSerializer.a("{\"text\": \"" + msg + "\"}");
            PacketPlayOutChat packetPlayOutChat = new PacketPlayOutChat(iChatBaseComponent, (byte) 2);
            ((CraftPlayer) p).getHandle().playerConnection.sendPacket(packetPlayOutChat);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public static String getShard(){
    	return Bukkit.getMotd().split(" ")[0];
    }
    
    /**
     * Get key from a HashMap with a value
     */
    public static <T, E> T getKeyByValue(Map<T, E> map, E value) {
        for (Entry<T, E> entry : map.entrySet()) {
            if (value.equals(entry.getValue())) {
                return entry.getKey();
            }
        }
        return null;
    }


    public static void refreshPlayerEquipment(Player player, Player forWhom) {

        if (forWhom.canSee(player) && player.getWorld().equals(forWhom.getWorld())) {
            final int id = player.getEntityId();
            final EntityHuman human = ((CraftPlayer) player).getHandle();
            final CraftPlayer otherGuyC = (CraftPlayer) forWhom;
            otherGuyC.getHandle().playerConnection.sendPacket(new PacketPlayOutEntityDestroy(id));
            otherGuyC.getHandle().playerConnection.sendPacket(new PacketPlayOutNamedEntitySpawn(human));
            otherGuyC.getHandle().playerConnection.sendPacket(new PacketPlayOutEntityEquipment(id, 0, CraftItemStack
                    .asNMSCopy(player.getItemInHand())));
            otherGuyC.getHandle().playerConnection.sendPacket(new PacketPlayOutEntityEquipment(id, 1, CraftItemStack
                    .asNMSCopy(player.getInventory().getBoots())));
            otherGuyC.getHandle().playerConnection.sendPacket(new PacketPlayOutEntityEquipment(id, 2, CraftItemStack
                    .asNMSCopy(player.getInventory().getLeggings())));
            otherGuyC.getHandle().playerConnection.sendPacket(new PacketPlayOutEntityEquipment(id, 3, CraftItemStack
                    .asNMSCopy(player.getInventory().getChestplate())));
            otherGuyC.getHandle().playerConnection.sendPacket(new PacketPlayOutEntityEquipment(id, 4, CraftItemStack
                    .asNMSCopy(player.getInventory().getHelmet())));
        }

    }
    
}
